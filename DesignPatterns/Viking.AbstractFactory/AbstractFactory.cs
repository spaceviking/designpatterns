﻿using Viking.AbstractFactory.Products.ProductA;
using Viking.AbstractFactory.Products.ProductB;

namespace Viking.AbstractFactory
{
    public abstract class AbstractFactory
    {
        public abstract AbstractProductA CreateProductA();
        public abstract AbstractProductB CreateProductB();
    }
}
