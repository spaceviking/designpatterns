﻿using Viking.AbstractFactory.Products.ProductA;
using Viking.AbstractFactory.Products.ProductB;

namespace Viking.AbstractFactory.Factories
{
    public class ConcreteFactory1 : AbstractFactory
    {
        public override AbstractProductA CreateProductA()
        {
            return new ProductA1();
        }

        public override AbstractProductB CreateProductB()
        {
            return new ProductB1();
        }
    }
}
