﻿using Viking.AbstractFactory.Products.ProductA;
using Viking.AbstractFactory.Products.ProductB;

namespace Viking.AbstractFactory.Factories
{
    public class ConcreteFactory2 : AbstractFactory
    {
        public override AbstractProductA CreateProductA()
        {
            return new ProductA2();
        }

        public override AbstractProductB CreateProductB()
        {
            return new ProductB2();
        }
    }
}
