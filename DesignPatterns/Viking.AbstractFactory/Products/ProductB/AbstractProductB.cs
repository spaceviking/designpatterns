﻿using Viking.AbstractFactory.Products.ProductA;

namespace Viking.AbstractFactory.Products.ProductB
{
    public abstract class AbstractProductB
    {
        public abstract void Interact(AbstractProductA abstractProductA);
    }
}
