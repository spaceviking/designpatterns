﻿using System;
using Viking.AbstractFactory.Products.ProductA;

namespace Viking.AbstractFactory.Products.ProductB
{
    public class ProductB2 : AbstractProductB
    {
        public override void Interact(AbstractProductA abstractProductA)
        {
            Console.WriteLine(this.GetType().Name + " interacts with " + abstractProductA.GetType().Name);
        }
    }
}
