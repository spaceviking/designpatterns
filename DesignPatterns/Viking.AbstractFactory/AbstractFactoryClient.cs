﻿using Viking.AbstractFactory.Products.ProductA;
using Viking.AbstractFactory.Products.ProductB;

namespace Viking.AbstractFactory
{
    public class AbstractFactoryClient
    {
        private AbstractProductA _abstractProductA;
        private AbstractProductB _abstractProductB;

        public AbstractFactoryClient(AbstractFactory factory)
        {
            _abstractProductA = factory.CreateProductA();
            _abstractProductB = factory.CreateProductB();
        }

        public void Run()
        {
            _abstractProductB.Interact(_abstractProductA);
        }
    }
}
