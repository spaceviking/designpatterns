﻿namespace Viking.Builder.Builder
{
    public class ConcreteBuilder1 : Builder
    {
        private Product _product= new Product();

        public override void BuildPartA()
        {
            _product.Add("PartA1");
        }

        public override void BuildPartB()
        {
            _product.Add("PartB1");
        }

        public override Product GetResult()
        {
            return _product;
        }
    }
}
